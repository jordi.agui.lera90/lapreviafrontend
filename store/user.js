
export const state = () => ({
    completed:[]
 })
 
 
 export const getters = {
    completed(state){
        return state.completed
    },
    completedGQL(state){
        let completed = []
        if(Array.isArray(state.completed)){
            completed = state.completed.map(fav => fav.id)
        }
        return completed
    }
  }
 
 export const mutations = {
    setCompleted(state, payload){
        state.completed = payload
    },
    // addProve(state, payload){
    //     state.completed.push(payload)
    // },
    // removeProve(state, id){
    //     const miProve = state.completed.find(prove => prove.id == id)
    //     const index = state.completed.indexOf(miProve)
    //     state.completed.splice(index, 1)
    // }
 }
 
 export const actions = {
   async getCompleted({commit}){
       let client = this.app.apolloProvider.defaultClient
       let id = this.$auth.user.id
       const query = {
           context:{
               headers:{
                   authorization:this.$auth.strategy.token.get()
               }
           },
           query:require("../graphql/userCompleted.gql"),
           fetchPolicy:"no-cache",
           variables:{id}
       }
       await client.query(query).then(data =>{
           commit("setCompleted", data.data.user.completed)
       }).catch(e => console.log(e))
   }
 }
 