export const strict = false

export const state = () => ({
    counter: 0,
    categories:[],
    loadedProves:[],
    teams: [],
    proves: [],
    // provesbycat: []
})


export const getters = {
    readCategories(state){
        return state.categories
    },
    readProves(state){
        return state.proves
    },
    // readProvesbyCat(state){
    //     return state.provesbycat
    // },
    readLoadedProves(state){
        return state.loadedProves
    },
    readCounter(state) {
        return state.counter
    },
    readTeams(state){
        return state.teams
    }
}

export const mutations = {
    addCategories(state, payload){
        state.categories = payload
    },
    addProves(state, payload){
        state.proves = payload
    },
    // addProvesbyCat(state, payload){
    //     state.provesbycat = payload
    // },
    addLoadedProves(state, payload){
        state.loadedProves = payload
    },
    increment(state) {
        state.counter++
    },
    addTeams(state, payload){
        state.teams = payload
    }
}

export const actions = {
   async nuxtServerInit({commit}) {
            const client = this.app.apolloProvider.defaultClient
            const query = {
                query: require("../graphql/categories.gql"),
                fetchPolicy: 'no-cache'
            }
            await client.query(query).then(data => {
                commit('addCategories', data.data.categories)
                commit('addLoadedProves', data.data.proves)
            }).catch(error => {
                console.log(error)
            })
            // const client2 = this.app.apolloProvider.defaultClient
            // const query2 = {
            //     query: require("../graphql/teamsData.gql")
            // }
            // await client2.query(query2).then(data => {
            //     commit("addTeams", data.data.users)
            // }).catch(error => {
            //     console.log(error)
            // })
            const client3 = this.app.apolloProvider.defaultClient
            const query3 = {
                query: require("../graphql/provesdata.gql")
            }
            await client3.query(query3).then(data => {
                commit("addProves", data.data.proves)
            }).catch(error => {
                console.log(error)
            })

    },
    refreshTeams({commit}){
        const client2 = this.app.apolloProvider.defaultClient
        const query2 = {
            query: require("../graphql/teamsData.gql"),
            fetchPolicy: 'no-cache'
        }
        client2.query(query2).then(data => {
            console.log(data)
            commit("addTeams", data.data.users)
        }).catch(e => console.log(e))
    },

    searchProve({commit}, payload){
        let client = this.app.apolloProvider.defaultClient
        const query = {
            query:require('../graphql/searchProve.gql'),
            variables:{term:payload}
        }
        client.query(query).then(data => {
            console.log(data)
            commit('addLoadedProves', data.data.proves)
        }).catch(e => console.log(e))
    },

    // provesbyCat({commit}){
    //     const slug = 'route.params.category'
    //     const client = app.apolloProvider.defaultClient
    //     const query = {
    //       query:require("../graphql/proves.gql"),
    //       variables:{slug}
    //     }
    //     client.query(query).then(data => {
    //     commit('addProvesbyCat', data.data.proves)
    //     }).catch(e => console.log(e))
    // },

    
    increment(context) {
        setTimeout(() => {
            context.commit("increment")
        }, 1000)
    }
}
