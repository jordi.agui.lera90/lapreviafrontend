export default function({store, redirect}){
    if(store.$auth.user.role.type !== 'admin'){
        redirect("/")
    }
}

